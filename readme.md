# SUBEDIT

Ever made the mistake of subscribing to Netflix abroad and getting stuck with nothing but native or japanese subtitles for animes? I guess not but here's a half-assed piece of python code that eased my suffering by adjusting the timing of *.dfxp* subtitles.

## Why
1) Japanese audio with german subtitles is just too cruel
2) Subtitles floating around the internet are usually out-of-sync. Additionally, the offsets appear to be different before and after intros for some reason, making adjustment even more painful.

## How
This project has two python scripts in the ```scripts``` directory (```processDir.py``` and ```processFile.py```) that handle pre-and post-intro subtitle timing adjustments for an entire directory or just a single file, respectively. Output subtitles are written to the original's directory with *_offset* tags slapped at the end of their names.

```
python3 path_to_script --help
```
The scripts should be called from a terminal in the following format:
```
python3 path_to_script path_to_file_or_directory -argument_name=argument_value ... -argument_name=argument_value
```

The following arguments are available:
* *introLine*: if the intro lyrics are translated as well, the value of this argument should be (a part of) the last line of the lyrics. This is the safest way to find the intro, but don't use it if the subtitles don't contain the lyrics.
* *introLength*: if no subtitles are shown during the intro, *introLength* should be set to the length of the intro (in seconds). Anime intros usually take 90 seconds. Keep in mind that the script will look for the first time gap in the subtitles that is at least *introLength* long, which might result in some unwanted behaviour, especially if not much is being said in the show.
* *introIndex*: a fallback option if nothing else works. Open the subtitle, find the first line after the intro, and set *introIndex* to the *id* of that line.
* *offset*: uniform time offset for both pre-and post-intro subtitles in seconds
* *preIntroOffset*: in seconds
* *postIntroOffset*: in seconds


## Prerequisites
* Python3
* Google Chrome
* [Super Netflix](https://chrome.google.com/webstore/detail/super-netflix/iakpdiefpdniabbekcbofaanjcpjkloe) Chrome extension for loading custom subtitles
* Source subtitle in *.dfxp* format

Having trouble finding subtitles? Try [these](https://www.cybrhome.com/topic/anime-subtitles) sites, especially [this one](https://kitsunekko.net/dirlist.php?dir=subtitles%2F). Of course, most subtitles will be in either *.ass* or *.srt* format so they'll need to be [converted](https://gotranscript.com/subtitle-converter) to *.dfxp*. This project might be extended in the future to include handling conversions as well, thus allowing to skip uploading anything.