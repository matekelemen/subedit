# --- External Imports ---
import xml.etree.ElementTree as xml
from xml.dom import minidom

# --- STL Imorts ---
import os


def prettyWrite( root, filePath ):
    string = xml.tostring(root)
    
    try:
        string = minidom.parseString( string ).toprettyxml(
            indent="    ",
            encoding="utf-8" )
    except Exception as exception:
        print( exception )
        tree = xml.ElementTree( root )
        tree.write( filePath, encoding="utf-8", xml_declaration=True )
        return

    with open( filePath, "wb" ) as file:
        file.write( string )




def makeEmptyDFXP( pretty=False ):

    # Templates
    kwargs = {
        "xmlns"     : "http://www.w3.org/ns/ttml",
        "xmlns:ttm" : "http://www.w3.org/ns/ttml#metadata",
        "xmlns:tts" : "http://www.w3.org/ns/ttml#styling",
        "xmlns:xsi" : "http://www.w3.org/2001/XMLSchema-instance"
    }

    # Make root node
    root = xml.Element( "tt", **kwargs )
    head = xml.SubElement( root, "head" )

    # Metadata
    metadata = xml.SubElement( head, "metadata" )
    xml.SubElement( metadata, "ttm:title" ).text = "Netflix Subtitle for phpT2ckBz"

    # Styling
    kwargs = {
        "tts:fontStyle"     : "normal",
        "tts:fintWeight"    : "normal",
        "xml:id"            : "s1",
        "tts:color"         : "white",
        "tts:fontFamily"    : "Arial",
        "tts:fontSize"      : "100%"
    }

    styling = xml.SubElement( head, "styling" )
    xml.SubElement( styling, "style", **kwargs ).text = ""

    # Layout
    kwargs = {
        "tts:extent"        : "80% 40%",
        "tts:origin"        : "10% 10%",
        "tts:displayAlign"  : "before",
        "tts:textAlign"     : "center",
        "xml:id"            : "topCenter"
    }

    layout = xml.SubElement( head, "layout" )
    xml.SubElement( layout, "region", **kwargs )

    if not pretty:
        kwargs["tts:origin"]       = "10% 50%"
        kwargs["tts:displayAlign"] = "after"
        kwargs["xml:id"]       = "bottomCenter"
        xml.SubElement( layout, "region", **kwargs )

    # Main
    kwargs = {
        "style"             :"s1",
        "xml:id"            : "d1"
    }

    body = xml.SubElement( root, "body" )
    xml.SubElement( body, "div", **kwargs )

    # DEBUG - write to file
    #print( xml.tostring( root ) )
    prettyWrite( root, "test.xml" )

    return root



def toDFXPTime( timeList ):
    """[hours,minutes,seconds] -> hours:minutes:seconds"""
    while len(timeList[0]) < 2:
        timeList[0] = "0" + timeList[0]

    while len(timeList[1]) < 2:
        timeList[1] = "0" + timeList[1]

    seconds = timeList[-1].split(".")
    while len(seconds[0]) < 2:
        seconds[0] = "0" + seconds[0]

    while len(seconds[1]) < 3:
        seconds[1] = seconds[1] + "0"

    return timeList[0] + ":" + timeList[1] + ":" + seconds[0] + "." + seconds[1]


def addSubtitleLine( divNode, id, contents ):
    kwargs = {
        "xml:id"    : "p" + str(id),
        "begin"     : toDFXPTime(contents["begin"].split(":")),
        "end"       : toDFXPTime(contents["end"].split(":")),
        "region"    : "bottomCenter"
    }
    xml.SubElement( divNode, "p", **kwargs ).text = contents["text"]




def parseASSLine( line ):
    """Dialogue: 0,timeBegin,timeEnd,styleName,subtitleType,0,0,0,,text"""
    line = line.split(",")[1:]
    contents = {
        "begin"     : line[0],
        "end"       : line[1],
        "text"      : line[8]
    }

    if len(line) > 9:
        contents["text"] = ",".join( line[8:] )

    contents["text"] = contents["text"].replace( "\\n", "<br/>" ).replace(  "\\N", "<br/>" )
    contents["text"] = contents["text"].replace( "{\\i0}", "" ).replace( "{\\i1}", "" )

    return contents




def parseSubtitleLine( line, format ):
    """Populate a dictionary with the contents of a subtitle line"""
    formatParsers = {
        "ass"   : parseASSLine
    }
    
    if not format in formatParsers:
        raise ValueError( "Unsupported format: " + format )

    return formatParsers[format](line)



def ass2dfxp( filePath ):
    root    = makeEmptyDFXP()
    divNode = root.find("body").find("div")
    counter = 0

    with open( filePath, "r" ) as file:
        for line in file:
            if line[:9] != "Dialogue:":
                continue
            else:
                counter += 1
                addSubtitleLine(
                    divNode,
                    counter,
                    parseSubtitleLine( line, "ass" )
                )

    filePath = os.path.splitext(filePath)[0] + ".dfxp"
    prettyWrite( root, filePath )




def convertSubtitle( filePath ):
    supportedSubtitleFormats = {
        ".ass"      : ass2dfxp,
        ".dfxp"     : lambda x: None
    }

    extension = os.path.splitext(filePath)[1]
    if not extension in supportedSubtitleFormats:
        raise ValueError( "Unsupported subtitle type for conversion: " + extension )
    
    return supportedSubtitleFormats[extension]( filePath )
