# --- STL Imports ---
import sys
import os


def parseCommandLineArguments( arguments ):
    # Initialize internals
    params          = { "path"              : [None, str],
                        "introLine"         : [None, str],
                        "introLength"       : [None, float],
                        "introIndex"        : [None, int],
                        "offset"            : [None, float],
                        "preIntroOffset"    : [0.0, float],
                        "postIntroOffset"   : [0.0, float],
                        "verbose"           : [False, bool]  }

    # Message definitions
    argumentNamesMessage = "available argument names:\n"
    for argumentName in list(params.keys())[1:]:
        argumentNamesMessage += "\t" + argumentName + "\n"

    argumentParseMessage = "expecting arguments of the following form: -argName=argVal\n"
    argumentParseMessage += "\n" + argumentNamesMessage

    # Help
    argv = arguments[1:]
    if len(argv)==0 or argv[0]=="--help":
        message = "\n--help\n"
        message += "python3 path_to_script path_to_file -argumentName=argumentValue ... -argumentName=argumentValue\n"
        print( message + argumentNamesMessage )
        sys.exit()

    # Parse file name
    if len(argv) == 0:
        raise RuntimeError( "No file name specified!" )
    else:
        params["path"][0] = argv[0]

    if not os.path.isabs(params["path"][0]):
        params["path"][0] = os.path.join( os.getcwd(), params["path"][0] )

    # Parse arguments: -argName=argValue
    argumentError = None

    for argument in argv[1:]:
        if argument[0] == "-":

            eqIndex = argument.find("=")
            if eqIndex == -1:
                argumentError = argument
                break

            argumentName    = argument[1:eqIndex]
            argumentValue   = argument[eqIndex+1:]
            if argumentName in params:
                try:
                    argumentValue       = params[argumentName][1](argumentValue)
                except:
                    argumentError       = argument
                    break
                params[argumentName][0] = argumentValue

        else:
            argumentError = argument

    if argumentError is not None:
        message = "Unable to parse argument: " + argumentError + "\n"
        raise RuntimeError( message + argumentParseMessage )


    if params["offset"][0] is not None:
        params["preIntroOffset"][0] = params["offset"][0]
        params["postIntroOffset"][0] = params["offset"][0]

    for item in params.items():
        message     = item[0]
        tabCounter  = 0
        while len(item[0]) + 4*tabCounter < 5 * round(max( [len(key) for key in params.keys()] ) / 4):
            message += "\t"
            tabCounter += 1
        message += ": "

        print( message.expandtabs(4), item[1][0] )
    print("\n")

    return params



class CommandLineParameters:
    """
    """
    def __init__(   self, 
                    defaultMap,
                    prefix="-",
                    valuePrefix="=" ):
        """
        """
        self.defaultMap     = defaultMap
        self.prefix         = prefix
        self.valuePrefix    = valuePrefix
        self.map            = self._convertMap( defaultMap.copy() )



    def parse( self, commandLineArguments ):
        """
        """
        parameters = self.map.copy()
        for name,value in self._convertArgumentsToMap(commandLineArguments).items():
            if name in parameters:
                parameters[name] = value
            else:
                raise ValueError( self.argumentErrorMessage )


    def _convertMap( self, parameterMap ):
        for name,value in parameterMap.items():
            parameterMap[name] = self.defaultMap[name][1](value)
        return parameterMap


    def _convertArgumentsToMap( self, commandLineArguments ):
        names   = []
        values  = []
        if self.valuePrefix != "":
            for argument in commandLineArguments:
                if not argument.startswith(self.prefix):
                    raise ValueError( self.argumentErrorMessage )
                nameBegin   = len(self.prefix)
                nameEnd     = argument.find(self.valuePrefix)

                if nameEnd == -1:
                    raise ValueError( self.argumentErrorMessage )

                valueBegin  = nameEnd + len(self.valuePrefix)
                valueEnd    = len(argument)

                names.append( argument[nameBegin:nameEnd] )
                values.append( argument[valueBegin:valueEnd] )

        else:
            if not len(commandLineArguments) == 0:
                names   = [ arg[len(self.prefix):] for arg in commandLineArguments[::2] ]
                values  = commandLineArguments[1::2]
      
        return self._convertMap(arguments)


    @property
    def argumentErrorMessage( self ):
        return ""