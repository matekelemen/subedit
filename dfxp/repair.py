# --- Internal Imports ---
from .query import findLike, findLike_dict
from .time import stringToTime24, time24ToSeconds

# --- STL Imports ---
import sys
from copy import deepcopy as copy


def repairID( root ):
    div         = findLike( "body", root )
    div         = findLike( "div", div )

    for index, child in enumerate(div):
        idTag               = findLike_dict( "id", child.attrib )
        child.attrib[idTag] = "p" + str(index+1)


def makeContinuous( root ):
    div         = findLike( "body", root )
    div         = findLike( "div", div )

    def sortKey( child ):
        return time24ToSeconds( stringToTime24( child.attrib["begin"] ) )

    div[:] = sorted( div, key=sortKey )


def eraseBracedText( root, braces="{}"):
    div         = findLike( "body", root )
    div         = findLike( "div", div )

    for child in div:
        if type(child.text) is str:
            string  = child.text
            begin   = string.find(braces[0])
            end     = string.find(braces[1])
            if begin!=-1 and end!=-1:
                string = string[:begin] + string[end+1:]
            child.text  = string
    