# --- External Imports ---
import xml.etree.ElementTree as xml

# --- Internal Imports ---
from .time import stringToTime24, time24ToString, addSeconds
from .query import findLike, getLike_dict

# --- STL Imports ---
import sys


def idRangeOffset( seconds, idMin=0, idMax=sys.maxsize ):
    def function(id):
        if idMin<=id and id<idMax:
            return seconds
        else:
            return 0.0
    return function


def combineShifts( functionList ):
    def function(ID):
        output = 0.0
        for f in functionList:
            output += f(ID)
        return output
    return function


def shiftByID( root, shiftFunction ):
    div = findLike( "body", root )
    div = findLike( "div", div )
    for child in div:
        id                      = int(getLike_dict("id",child.attrib)[1:])
        offset                  = shiftFunction(id)

        begin                   = child.attrib[ "begin" ]
        beginTime               = stringToTime24( begin )
        beginTime               = addSeconds( beginTime, offset )
        begin                   = time24ToString( beginTime )

        end                     = child.attrib[ "end" ]
        endTime                 = stringToTime24( end )
        endTime                 = addSeconds( endTime, offset )
        end                     = time24ToString( endTime )

        child.attrib["begin"]   = begin
        child.attrib["end"]     = end