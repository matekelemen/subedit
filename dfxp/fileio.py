# --- External Imports ---
import xml.etree.ElementTree as xml


def parse( fileName ):
    tree = xml.ElementTree()
    tree.parse( fileName )
    return tree