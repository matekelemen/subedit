# --- Internal Imports ---
import dfxp

# --- STL Imports ---
import sys
import os


def processFile( params ):
    # List available subtitles in the specified directory
    filePath = params["path"][0]

    # Check extension and offset tag
    if os.path.splitext(filePath)[1] != ".dfxp" or ("_offset" in filePath):
        if params["verbose"]:
            print( "ignored: " + filePath )
        return

    # Process subtitles
    if params["verbose"]:
        print( "processing: " + os.path.basename(filePath) )

    # Read subtitle
    tree = dfxp.fileio.parse(filePath)
    root = tree.getroot()

    # Repair indices
    dfxp.makeContinuous( root )
    dfxp.repairID( root )

    # Find intro
    introIndex = -1
    while params["offset"][0] is None:

        # Find intro index - by lyrics
        if params["introLine"][0] is not None:
            if params["verbose"]:
                print( "Searching for intro by lyrics..." )
            introIndex, line = dfxp.findLineID( params["introLine"][0], root )

        if introIndex != None and introIndex > -1:
            introIndex += 1
            if params["verbose"]:
                print( "Found intro index %i" % introIndex )
            break


        # Find intro index - by intro length
        if params["introLength"][0] is not None:
            if params["verbose"]:
                print( "Searching for intro by lack of subtitles..." )
            try:
                introIndex = dfxp.findIntroIndex( root, introLength=params["introLength"][0], verbose=True )
            except StopIteration:
                pass

        if introIndex != None and introIndex > -1:
            if params["verbose"]:
                print( "Found intro index %i" % introIndex )
            break

        # User-specified intro index
        if params["introIndex"][0] is not None:
            introIndex = params["introIndex"][0]
            if params["verbose"]:
                print( "Forcing intro index: " + str(introIndex) )

        # Exit without finding the intro index
        introIndex = sys.maxsize
        if params["verbose"]:
            print( "Failed to find intro index, using default: " + str(introIndex) )
        break

    # No intro index found
    if introIndex == -1:
        introIndex = sys.maxsize

    # Offset subtitles
    shiftFunc = dfxp.combineShifts([    dfxp.idRangeOffset( params["preIntroOffset"][0], idMax=introIndex),
                                        dfxp.idRangeOffset( params["postIntroOffset"][0], idMin=introIndex) ])


    if params["verbose"]:
        print( "Offsetting subtitles" )
    try:
        dfxp.shift.shiftByID( root, shiftFunc )
    except:
        if params["verbose"]:
            print( "Failed to offset subtitles" )

    # Remove braced texts
    if params["verbose"]:
        print("Remove braced texts")
    dfxp.repair.eraseBracedText(root)


    # Output
    if params["verbose"]:
        print( "Writing modified subtitles" )
    outFileName = os.path.splitext( filePath )
    outFileName = outFileName[0] + "_offset" + outFileName[1]
    tree.write(outFileName)

    if params["verbose"]:
        print("done\n")