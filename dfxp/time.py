def stringToTime24( string ):
    time    = [0,0,0.0]

    # hours
    iH      = string.find(":")
    time[0] = int( string[:iH] )

    # minutes
    iM      = string[iH+1:].find(":") + iH+1
    time[1] = int( string[iH+1:iM] )

    # seconds
    time[2] = float( string[iM+1:] )

    return time


def time24ToString( time ):

    def formatInt( num ):
        if num>60 or num<0:
            raise ValueError( "Invalid int time format: " + str(num) )
        string = str(num)
        if len(string) < 2:
            while len(string) < 2:
                string = "0" + string
        return string

    def formatFloat( num ):
        if num>60.0 or num<0.0:
            raise ValueError( "Invalid float time format: " + str(num) )
        string  = "%.2f" % num
        index   = string.find(".")
        while index < 2:
            string  = "0" + string
            index   += 1
        while len(string)-(index+1)<3:
            string  += "0"
        return string

    return formatInt(time[0]) + ":" + formatInt(time[1]) + ":" + formatFloat(time[2])


def time24ToSeconds( time ):
    return 3600.0*time[0] + 60.0*time[1] + time[2]


def addSeconds( time, seconds ):
    time[2]     += seconds
    overflow    = int( time[2]/60.0 )
    if time[2]<0.0:
        overflow -= 1
    time[2]     -= 60.0*overflow

    time[1]     += overflow
    overflow    = int( time[1]/60.0 )
    if time[1]<0:
        overflow -= 1
    time[1]     -= 60*overflow

    time[0]     += overflow
    overflow    = int( time[0]/60.0 )
    if overflow > 0:
        raise ValueError( "24h time representation overflow!" )

    return time