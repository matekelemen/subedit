# --- Internal Imports ---
from .time import stringToTime24, time24ToSeconds


def findLike( tag, node ):
    for child in node:
        if tag in child.tag:
            return child
    return None


def getLike( tag, node ):
    for child in node:
        if tag in child.tag:
            return child.attrib
    return None


def findLike_dict( tag, dictionary ):
    for key in dictionary.keys():
        if tag in key:
            return key
    return None


def getLike_dict( tag, dictionary):
    for key in dictionary.keys():
        if tag in key:
            return dictionary[key]
    return None

def findIntroIndex( root, introLength=90.0, verbose=True ):
    div         = findLike( "body", root )
    div         = findLike( "div", div )
    lastBegin   = None

    for index, child in enumerate(div):
        begin = stringToTime24( child.attrib["begin"] )
        begin = time24ToSeconds(begin)

        if lastBegin is None:
            lastBegin = begin

        if begin - lastBegin >= introLength:
            if verbose:
                print( "Intro index found (" + str(index+1) + ")" )
            return index+1
        lastBegin = begin

    if verbose:
        print( "Intro index not found!" )
    raise StopIteration


def findLineID( string, root ):
    div         = findLike( "body", root )
    div         = findLike( "div", div )

    for index, child in enumerate(div):
        if child.text is None:
            continue
        if string in child.text:
            return index+1, child.text
    
    return None, None