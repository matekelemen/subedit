# --- Internal Imports ---
from dfxp.conversion import convertSubtitle

# --- STL Imports ---
import sys
import os


if __name__ == "__main__":

    if len(sys.argv) < 2:
        raise ValueError( "Specify path to a file to convert" )

    filePath = sys.argv[1]
    if not os.path.isfile( filePath ):
        raise ValueError( "Expecting a file, got: " + filePath )

    convertSubtitle( filePath )