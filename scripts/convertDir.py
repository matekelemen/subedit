# --- Internal Imports ---
from dfxp.conversion import convertSubtitle

# --- STL Imports ---
import sys
import os


if __name__ == "__main__":

    if len(sys.argv) < 2:
        raise ValueError( "Specify path to a file to convert" )

    dirPath = sys.argv[1]
    if not os.path.isdir( dirPath ):
        raise ValueError( "Expecting a directory, got: " + dirPath )

    # Get all files in the directory
    filePaths = [os.path.join(dirPath, f) for f in os.listdir(dirPath) if os.path.isfile(os.path.join(dirPath, f))]

    for filePath in filePaths:
        convertSubtitle( filePath )