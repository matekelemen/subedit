# --- DFXP Imports ---
from dfxp.commandline import CommandLineParameters as Parameters

# --- STL Imports ---
import sys
import os


class DirectoryPreprocessor:
    """
    """
    def __init__( self, commandLineArguments ):
        if len(commandLineArguments) < 2:
            print( self.help )
            raise RuntimeError( "Invalid construction of DirectoryPreprocessor" )
        
        self.path = commandLineArguments[1]
        if not os.path.isdir( self.path ):
            raise ValueError( self.path + " is not a directory!" )

        defaultParameters = {
            "verbose"   : ("True",bool),
            "force"     : ("",bool)
        }
        self.params = Parameters(defaultParameters).parse(commandLineArguments[1:])

        self.verbose = True
        if len(commandLineArguments) > 2 and commandLineArguments[2]=="-verbose=false":
            self.verbose = False


    @property
    def help( self ):
        helpString = """
        scripts/preprocessDir.py <directory_path>
        Collect all files in directory_path and rename the files to ep_i in alphabetical order,
        then silently run scripts/processFile.py on all .dfxp files without performing any shifts.
        """
        return helpString


    def __call__( self, force=False ):
        # Collect .dfxp files in the directory
        dfxpSubtitles = []
        if force:
            dfxpSubtitles = [ f for f in os.listdir(self.path) if os.path.isfile(os.path.join(self.path,f)) ]
        else:
            dfxpSubtitles = [ f for f in os.listdir(self.path) if os.path.isfile(os.path.join(self.path,f)) and f.endswith(".dfxp") ]
        
        # Rename files
        counter = 1
        for file in dfxpSubtitles:
            if force or not os.path.splitext(file)[0].endswith("_offset"):
                newName = "ep_" + str(counter)
                os.rename(  os.path.join( self.path, file ),
                            os.path.join(self.path, newName) )
                counter += 1

                if self.params["verbose"]:
                    print( newName + " <-- " + file )


if __name__ == "__main__":
    processor = DirectoryPreprocessor(sys.argv)
    processor()