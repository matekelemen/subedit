# --- Internal Imports ---
import dfxp

# --- STL Imports ---
import sys
import os


if __name__ == "__main__":

    # Initialize internals
    params = dfxp.parseCommandLineArguments(sys.argv)

    # List available subtitles in the specified directory
    filePaths = [os.path.join(params["path"][0], f) for f in os.listdir(params["path"][0]) if os.path.isfile(os.path.join(params["path"][0], f))]

    # Get rid of non-dfxp files
    temp = []
    for filePath in filePaths:
        if os.path.splitext(filePath)[1] == ".dfxp" and not ("_offset" in filePath):
            temp.append(filePath)

    filePaths = temp
    if len(filePaths) == 0:
        print( "Found no processable files!" )


    # Process subtitles
    for filePath in filePaths:
        params["path"][0] = filePath
        dfxp.processFile(params)