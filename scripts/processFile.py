# --- Internal Imports ---
import dfxp

# --- STL Imports ---
import sys
import os


if __name__ == "__main__":
    # Initialize internals
    params = dfxp.parseCommandLineArguments(sys.argv)
    dfxp.processFile(params)